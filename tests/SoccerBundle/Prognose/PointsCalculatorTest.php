<?php
namespace SoccerBundle\Prognose;

class PointsCalculatorTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var PointsCalculator
     */
    protected $pointsCalculator;

    public function setup()
    {
        $this->pointsCalculator = new PointsCalculator();
    }

    public function testCorrectPrognose()
    {
        $points = $this->pointsCalculator->calculate(3, 1, 3, 1);
        $this->assertEquals(3, $points);
    }

    public function testLostHomePrognose()
    {
        $points = $this->pointsCalculator->calculate(3, 1, 1, 3);
        $this->assertEquals(0, $points);
    }

    public function testCorrectResultWinHomeTeamPrognose()
    {
        $points = $this->pointsCalculator->calculate(3, 1, 2, 0);
        $this->assertEquals(1, $points);
    }

    public function testCorrectResultWinAwayTeamPrognose()
    {
        $points = $this->pointsCalculator->calculate(1, 2, 0, 2);
        $this->assertEquals(1, $points);
    }

    public function testCorrectResultDrawPrognose()
    {
        $points = $this->pointsCalculator->calculate(0, 0, 2, 2);
        $this->assertEquals(1, $points);
    }
}
