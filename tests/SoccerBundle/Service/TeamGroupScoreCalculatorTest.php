<?php
namespace SoccerBundle\Service;

use SoccerBundle\Entity\Match;
use SoccerBundle\GroupStage\TeamGroupScoreCalculator;
use TeamBundle\Entity\Team;

class TeamGroupScoreCalculatorTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var TeamGroupScoreCalculator
     */
    protected $teamGroupScoreCalculator;

    public function setup()
    {
        $pointsCalculator = $this->getMockBuilder('SoccerBundle\GroupStage\PointsCalculator')
            ->setMethods(['calculate'])
            ->getMock();

        $pointsCalculator
            ->method('calculate')
            ->will($this->returnValueMap([[1, 3, 0], [3, 3, 1], [3, 1, 3]]));

        $this->teamGroupScoreCalculator = new TeamGroupScoreCalculator($pointsCalculator);
    }

    /**
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    private function getTeam()
    {
        $team = $this->getMockBuilder('SoccerBundle\Entity\SoccerTeam')
            ->setMethods(['getAllGroupMatches'])
            ->getMock();

        return $team;
    }

    /**
     * @param Team $team
     * @return Match
     */
    private function getWonMatchToTeam(Team $team)
    {
        return $this->addMatch($team, 3, 1);
    }

    /**
     * @param Team $team
     * @return Match
     */
    private function getLostMatchToTeam(Team $team)
    {
        return $this->addMatch($team, 1, 3);
    }

    /**
     * @param Team $team
     * @return Match
     */
    private function getDrawMatchToTeam(Team $team)
    {
        return $this->addMatch($team, 3, 3);
    }

    /**
     * @param Team $team
     * @param $homeScore
     * @param $awayScore
     *
     * @return Match
     */
    private function addMatch(Team $team, $homeScore, $awayScore)
    {
        $match = $this->getMockBuilder('SoccerBundle\Entity\Match')
            ->setMethods(['getHomeTeam', 'isEnded', 'getScoreHome', 'getScoreAway'])
            ->getMock();

        $match->method('isEnded')->willReturn(true);
        $match->method('getScoreHome')->willReturn($homeScore);
        $match->method('getScoreAway')->willReturn($awayScore);

        $match->method('getHomeTeam')->willReturn($team);

        return $match;
    }

    public function testEverythingIsEmptyWhenGroups()
    {
        $team = $this->getTeam();
        $team->method('getAllGroupMatches')->willReturn([]);
        $teamGroupScore = $this->teamGroupScoreCalculator->calculate($team);

        $this->assertEquals(0, $teamGroupScore->getDraws());
        $this->assertEquals(0, $teamGroupScore->getLoses());
        $this->assertEquals(0, $teamGroupScore->getWins());
        $this->assertEquals(0, $teamGroupScore->getGoalDifference());
        $this->assertEquals(0, $teamGroupScore->getGoalsAgainst());
        $this->assertEquals(0, $teamGroupScore->getGoalsFor());
        $this->assertEquals(0, $teamGroupScore->getPlayed());
        $this->assertEquals(0, $teamGroupScore->getPoints());
    }

    public function testTeamPlayedAndWonOneMatch()
    {
        $team = $this->getTeam();
        $team->method('getAllGroupMatches')->willReturn([
            $this->getWonMatchToTeam($team)
        ]);

        $teamGroupScore = $this->teamGroupScoreCalculator->calculate($team);

        $this->assertEquals(0, $teamGroupScore->getDraws());
        $this->assertEquals(0, $teamGroupScore->getLoses());
        $this->assertEquals(1, $teamGroupScore->getWins());
        $this->assertEquals(2, $teamGroupScore->getGoalDifference());
        $this->assertEquals(1, $teamGroupScore->getGoalsAgainst());
        $this->assertEquals(3, $teamGroupScore->getGoalsFor());
        $this->assertEquals(1, $teamGroupScore->getPlayed());
        $this->assertEquals(3, $teamGroupScore->getPoints());
    }

    public function testTeamPlayedAndDrawOneMatch()
    {
        $team = $this->getTeam();
        $team->method('getAllGroupMatches')->willReturn([
            $this->getDrawMatchToTeam($team)
        ]);

        $teamGroupScore = $this->teamGroupScoreCalculator->calculate($team);

        $this->assertEquals(1, $teamGroupScore->getDraws());
        $this->assertEquals(0, $teamGroupScore->getLoses());
        $this->assertEquals(0, $teamGroupScore->getWins());
        $this->assertEquals(0, $teamGroupScore->getGoalDifference());
        $this->assertEquals(3, $teamGroupScore->getGoalsAgainst());
        $this->assertEquals(3, $teamGroupScore->getGoalsFor());
        $this->assertEquals(1, $teamGroupScore->getPlayed());
        $this->assertEquals(1, $teamGroupScore->getPoints());
    }

    public function testTeamPlayedAndLostOneMatch()
    {
        $team = $this->getTeam();
        $team->method('getAllGroupMatches')->willReturn([
            $this->getLostMatchToTeam($team)
        ]);

        $teamGroupScore = $this->teamGroupScoreCalculator->calculate($team);

        $this->assertEquals(0, $teamGroupScore->getDraws());
        $this->assertEquals(1, $teamGroupScore->getLoses());
        $this->assertEquals(0, $teamGroupScore->getWins());
        $this->assertEquals(-2, $teamGroupScore->getGoalDifference());
        $this->assertEquals(3, $teamGroupScore->getGoalsAgainst());
        $this->assertEquals(1, $teamGroupScore->getGoalsFor());
        $this->assertEquals(1, $teamGroupScore->getPlayed());
        $this->assertEquals(0, $teamGroupScore->getPoints());
    }
}
