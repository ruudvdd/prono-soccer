<?php
namespace SoccerBundle\Service;

use SoccerBundle\GroupStage\PointsCalculator;

class PointsCalculatorTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var PointsCalculator
     */
    protected $pointsCalculator;

    public function setup()
    {
        $this->pointsCalculator = new PointsCalculator();
    }

    public function testDraw()
    {
        $this->assertEquals(1, $this->pointsCalculator->calculate(2, 2));
    }

    public function testWin()
    {
        $this->assertEquals(3, $this->pointsCalculator->calculate(3, 1));
    }

    public function testLoss()
    {
        $this->assertEquals(0, $this->pointsCalculator->calculate(1, 3));
    }
}
