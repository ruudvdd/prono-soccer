<?php
namespace TeamBundle\Entity;

use AppBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\MappedSuperclass()
 */
class Prognose
{
    /**
     * @var Match
     * @ORM\Id()
     * @ORM\JoinColumn(name="match_id")
     * @ORM\ManyToOne(targetEntity="TeamBundle\Entity\Match")
     */
    protected $match;

    /**
     * @var User
     * @ORM\Id()
     * @ORM\JoinColumn(name="user_id")
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="prognoses")
     */
    protected $user;

    /**
     * @return Match
     */
    public function getMatch()
    {
        return $this->match;
    }

    /**
     * @param Match $match
     * @return $this
     */
    public function setMatch($match)
    {
        $this->match = $match;
        return $this;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return $this
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }
}
