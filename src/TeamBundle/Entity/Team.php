<?php
namespace TeamBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\MappedSuperclass()
 */
class Team
{
    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=250)
     */
    protected $name;

    /**
     * @var string
     * @ORM\Column(type="string", length=100, name="image_class")
     */
    protected $imageClass;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getImageClass()
    {
        return $this->imageClass;
    }

    /**
     * @param string $imageClass
     * @return $this
     */
    public function setImageClass($imageClass)
    {
        $this->imageClass = $imageClass;
        return $this;
    }
}
