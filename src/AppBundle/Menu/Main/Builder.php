<?php
namespace AppBundle\Menu\Main;

use Knp\Menu\FactoryInterface;

class Builder
{
    private $factory;

    /**
     * @param FactoryInterface $factory
     */
    public function __construct(FactoryInterface $factory)
    {
        $this->factory = $factory;
    }

    public function createMainMenu(array $options)
    {
        $menu = $this->factory->createItem('root', [
            'childrenAttributes' => [
                'class' => 'navigation'
            ]
        ]);

        $menu->addChild('Dashboard', [
            'route' => 'homepage',
            'attributes' => [
                'class' => 'navigation__item'
            ]
        ]);

        $menu->addChild('Mijn pronostiek', [
            'route' => 'my-prono',
            'attributes' => [
                'class' => 'navigation__item'
            ]
        ]);

        return $menu;
    }
}
