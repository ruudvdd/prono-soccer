<?php
namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Entity\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use SoccerBundle\Entity\Match;
use SoccerBundle\Entity\Prognose;

/**
 * @ORM\Entity()
 * @ORM\Table(name="users")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var ArrayCollection $prognoses
     * @ORM\OneToMany(targetEntity="SoccerBundle\Entity\Prognose", mappedBy="user")
     */
    protected $prognoses;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    protected $score;

    public function __construct()
    {
        $this->prognoses = new ArrayCollection();
        parent::__construct();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return array
     */
    public function getPrognoses()
    {
        return $this->prognoses->toArray();
    }

    /**
     * @param Match $match
     * @return null|Prognose
     */
    public function getPrognose(Match $match)
    {
        $prognoses = $this->prognoses;
        $iterator = $prognoses->getIterator();
        while ($prognose = $iterator->current()) {
            /** @var Prognose $prognose */
            if ($match === $prognose->getMatch()) {
                return $prognose;
            }
            $iterator->next();
        }
        return null;
    }

    /**
     * @param array $prognoses
     * @return $this
     */
    public function setPrognoses(array $prognoses)
    {
        $this->prognoses = new ArrayCollection($prognoses);
        return $this;
    }

    /**
     * @return int
     */
    public function getScore()
    {
        return $this->score;
    }

    /**
     * @param int $score
     * @return $this
     */
    public function setScore($score)
    {
        $this->score = $score;
        return $this;
    }
}