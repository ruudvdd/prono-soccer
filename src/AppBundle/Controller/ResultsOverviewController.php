<?php
namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use AppBundle\Entity\User;

class ResultsOverviewController extends Controller
{
    /**
     * @Route("/my-prono", name="my-prono")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $groups = $em->getRepository('SoccerBundle:Group')->findAll();

        $groupScoreBoards = [];
        foreach ($groups as $group) {
            $groupScoreBoards[] = $this->get('soccer.group_stage.group_score_calculator')
                ->calculate($group);
        }

        $pointsCalculator = $this->get('soccer.prognose.points_calculator');

        return $this->render('default/my-prono.html.twig', [
            'groupScoreBoards' => $groupScoreBoards,
            'groups' => $groups,
            'pointsCalculator' => $pointsCalculator
        ]);
    }
}
