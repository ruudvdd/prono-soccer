<?php
namespace AppBundle\Controller;

use SoccerBundle\Entity\Match;
use SoccerBundle\Form\Prognose;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use AppBundle\Entity\User;
use SoccerBundle\Entity\Prognose as PrognoseEntity;

class MatchController extends Controller
{
    /**
     * @Route(
     *  "/matches/{match}",
     *  name="match_detail",
     *  requirements={
     *     "match_id": "\d+",
     *  }
     * )
     *
     * @param Request $request
     * @param Match $match
     * @return Response
     */
    public function prognoseMatch(Request $request, Match $match)
    {
        $em = $this->getDoctrine()->getManager();
        $userId = $request->query->get('user_id');
        $user = null;

        // get user
        if ($userId !== null) {
            $user = $em->getRepository('TeamBundle:User')->find($userId);
        }

        if ($user === null) {
            $user = $this->getUser();
        }

        // get prognose
        $prognoseRepository = $em->getRepository('SoccerBundle:Prognose');

        $prognose = $prognoseRepository->findOneBy([
            'user' => $user,
            'match' => $match
        ]);

        $parameters = ['match' => $match];
        if ($prognose === null && $user !== null) {
            $prognose = $this->createNewPrognose($match, $user);
        }

        $parameters['prognose'] = $prognose;

        $matchDateTime = $match->getDateTime();
        $deadline = $matchDateTime->sub(new \DateInterval('PT5M'));
        $now = new \DateTime();
        if ($user !== null && $this->isMe($user) && $deadline > $now && !$match->isEnded()) {
            $response = $this->handlePrognoseForm($request, $prognose, $parameters);
            if ($response instanceof Response) {
                return $response;
            }

            return $this->render('default/match-form.html.twig', $parameters);
        }

        $parameters['pointsCalculator'] = $this->get('soccer.prognose.points_calculator');
        return $this->render('default/match-detail.html.twig', $parameters);
    }

    /**
     * @param Match $match
     * @param User $user
     * @return PrognoseEntity
     */
    private function createNewPrognose(Match $match, User $user)
    {
        $prognose = new PrognoseEntity();
        $prognose->setMatch($match);
        $prognose->setUser($user);
        return $prognose;
    }

    /**
     * @param Request $request
     * @param PrognoseEntity $prognose
     * @param $parameters
     * @return bool|Response
     */
    private function handlePrognoseForm(Request $request, PrognoseEntity $prognose, &$parameters)
    {
        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm(Prognose::class, $prognose);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($prognose);
            $em->flush();
            $this->addFlash('success', 'Pronostiek opgeslagen');
            return RedirectResponse::create($this->get('router')->generate('my-prono'));
        }

        $parameters['form'] = $form->createView();
        return true;
    }

    /**
     * @param User $user
     * @return bool
     */
    private function isMe(User $user)
    {
        return $this->getUser() === $user;
    }
}
