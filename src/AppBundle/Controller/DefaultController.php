<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $repository = $this->get('doctrine')->getRepository('AppBundle:User');
        $matchRepository = $this->get('doctrine')->getRepository('SoccerBundle:Match');
        $users = $repository->findBy([], ['score' => 'DESC']);

        $nextMatches = $matchRepository->getNextMatches();
        $pointsCalculator = $this->get('soccer.prognose.points_calculator');

        return $this->render('default/index.html.twig', array(
            'users' => $users,
            'nextMatches' => $nextMatches,
            'pointsCalculator' => $pointsCalculator
        ));
    }

    /**
     * @Route("/group-matches-tool", name="")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function groupMatchesToolAction(Request $request)
    {
        $away = $request->query->getBoolean('away', false);

        // get all groups
        $groupRepository = $this->get('doctrine')->getRepository('SoccerBundle:Group');
        $groups = $groupRepository->findAll();

        // get all matches that need to be filled in
        $matchPersistTool = $this->get('soccer.match_persist_tool');
        $matches = $matchPersistTool->getAllMatchesForGroups($groups, $away);

        // post filled in data
        if ($request->isMethod('post')) {
            $matchPersistTool->fillInAndPersistGroupMatches($matches, $request->request->all(), $away);
        }

        return $this->render('default/matches-tool.html.twig', [
            'matches' => $matches,
            'away' => $away
        ]);
    }

    /**
     * @Route("/group-standings", name="group-standings")
     * @param Request $request
     * @return Response
     */
    public function groupStageAction(Request $request)
    {
        $groups = $this->getDoctrine()->getRepository('SoccerBundle:Group')->findAll();
        $groupScoreBoards = [];
        foreach ($groups as $group) {
            $groupScoreBoards[] = $this->get('soccer.group_stage.group_score_calculator')
                ->calculate($group);
        }

        return $this->render('default/group-standings.html.twig', [
            'groupScoreBoards' => $groupScoreBoards
        ]);
    }
}
