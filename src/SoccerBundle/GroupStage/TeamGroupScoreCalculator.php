<?php
namespace SoccerBundle\GroupStage;

use SoccerBundle\Entity\Match;
use SoccerBundle\Entity\SoccerTeam;
use SoccerBundle\Value\TeamGroupScore;

class TeamGroupScoreCalculator
{
    /**
     * @var PointsCalculator
     */
    protected $pointsCalculator;

    /**
     * @param PointsCalculator $pointsCalculator
     */
    public function __construct(PointsCalculator $pointsCalculator)
    {
        $this->pointsCalculator = $pointsCalculator;
    }

    /**
     * @param SoccerTeam $team
     * @return TeamGroupScore
     */
    public function calculate(SoccerTeam $team)
    {
        /** @var Match[] $matches */
        $groupMatches = $team->getAllGroupMatches();

        $groupScore = (new TeamGroupScore())
            ->setTeam($team)
            ->setGoalDifference(0)
            ->setGoalsAgainst(0)
            ->setGoalsFor(0)
            ->setLoses(0)
            ->setDraws(0)
            ->setWins(0)
            ->setPlayed(0)
            ->setPoints(0);

        foreach ($groupMatches as $match) {
            if (!$match->isEnded()) {
                continue;
            }

            $this->addScoresOfMatch($team, $match, $groupScore);
        }

        return $groupScore;
    }

    /**
     * @param SoccerTeam $team
     * @param Match $match
     * @param TeamGroupScore $groupScore
     */
    protected function addScoresOfMatch(SoccerTeam $team, Match $match, TeamGroupScore $groupScore)
    {
        $teamScore = intval($team === $match->getHomeTeam() ? $match->getScoreHome() : $match->getScoreAway());
        $opponentScore = intval($team === $match->getHomeTeam() ? $match->getScoreAway() : $match->getScoreHome());

        $loss = $teamScore < $opponentScore;
        $win = $teamScore > $opponentScore;
        $draw = $teamScore === $opponentScore;

        $groupScore
            ->addGoalDifference($teamScore - $opponentScore)
            ->addGoalsAgainst($opponentScore)
            ->addGoalsFor($teamScore)
            ->addLoses($loss ? 1 : 0)
            ->addDraws($draw ? 1 : 0)
            ->addWins($win ? 1 : 0)
            ->addPlayed(1)
            ->addPoints($this->pointsCalculator->calculate($teamScore, $opponentScore));
    }
}
