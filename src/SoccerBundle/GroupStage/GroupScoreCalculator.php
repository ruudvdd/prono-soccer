<?php
namespace SoccerBundle\GroupStage;

use SoccerBundle\Entity\Group;
use SoccerBundle\Value\GroupScoreBoard;

class GroupScoreCalculator
{
    /**
     * @var TeamGroupScoreCalculator
     */
    protected $teamGroupScoreCalculator;

    /**
     * @param TeamGroupScoreCalculator $teamGroupScoreCalculator
     */
    public function __construct(TeamGroupScoreCalculator $teamGroupScoreCalculator)
    {
        $this->teamGroupScoreCalculator = $teamGroupScoreCalculator;
    }

    /**
     * @param Group $group
     * @return GroupScoreBoard
     */
    public function calculate(Group $group)
    {
        $scores = [];
        $teams = $group->getTeams();

        foreach ($teams as $team) {
            $scores[] = $this->teamGroupScoreCalculator->calculate($team);
        }

        $groupScoreBoard = new GroupScoreBoard();
        $groupScoreBoard->setGroupScores($scores);
        $groupScoreBoard->setGroup($group);
        return $groupScoreBoard;
    }
}
