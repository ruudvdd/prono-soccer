<?php
namespace SoccerBundle\GroupStage;

class PointsCalculator
{
    /**
     * @param int $teamScore
     * @param int $opponentScore
     * @return int
     */
    public function calculate($teamScore, $opponentScore)
    {
        $teamScore = intval($teamScore);
        $opponentScore = intval($opponentScore);

        return $teamScore > $opponentScore ? 3 : ($teamScore < $opponentScore ? 0 : 1);
    }
}
