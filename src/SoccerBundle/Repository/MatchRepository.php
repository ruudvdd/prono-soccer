<?php

namespace SoccerBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use SoccerBundle\Entity\Match;
use SoccerBundle\Entity\SoccerTeam;

/**
 *
 */
class MatchRepository extends EntityRepository
{
    /**
     * @param SoccerTeam $homeTeam
     * @param SoccerTeam $awayTeam
     * @param bool $groupMatch make sure it's a group match
     * @return Match
     */
    public function findBetween(SoccerTeam $homeTeam, SoccerTeam $awayTeam, $groupMatch = true)
    {
        return $this->createQueryBuilder('m')
            ->select('m')
            ->andWhere('m.homeTeam=:home_team')
            ->andWhere('m.awayTeam=:away_team')
            ->andWhere('m.group=:group')
            ->setParameters([
                ':home_team' => $homeTeam,
                ':away_team' => $awayTeam,
                ':group' => $groupMatch
            ])
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @return Match[]
     */
    public function findAllEnded()
    {
        return $this->createQueryBuilder('m')
            ->select('m')
            ->andWhere('m.ended=1')
            ->getQuery()
            ->getResult();
    }

    public function getNextMatches()
    {
        $queryBuilder = $this->createQueryBuilder('m');
        $nextMatchDate = $this->getNextMatchDate();

        return $queryBuilder
            ->select('m')
            ->andWhere('m.date=:next_match_date')
            ->setParameter(':next_match_date', $nextMatchDate)
            ->addOrderBy('m.time', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    protected function getNextMatchDate()
    {
        $nowDateTime = new \DateTimeImmutable();
        $nextMatchDate = $this->createQueryBuilder('m2')
            ->select('m2.date')
            ->andWhere('m2.date >= :now')
            ->addOrderBy('m2.date', 'ASC')
            ->addOrderBy('m2.time', 'ASC')
            ->setParameter(':now', $nowDateTime->format('Y-m-d'))
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult(Query::HYDRATE_SINGLE_SCALAR);
        return $nextMatchDate;
    }
}
