<?php
namespace SoccerBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use TeamBundle\Entity\Team;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="teams")
 */
class SoccerTeam extends Team
{
    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="SoccerBundle\Entity\Match", mappedBy="homeTeam")
     */
    protected $homeMatches;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="SoccerBundle\Entity\Match", mappedBy="awayTeam")
     */
    protected $awayMatches;

    /**
     * @return Match[]
     */
    public function getHomeMatches()
    {
        return $this->homeMatches->toArray();
    }

    /**
     * @param Match[] $homeMatches
     * @return $this
     */
    public function setHomeMatches(array $homeMatches)
    {
        $this->homeMatches = new ArrayCollection($homeMatches);
        return $this;
    }

    /**
     * @return Match[]
     */
    public function getAwayMatches()
    {
        return $this->awayMatches->toArray();
    }

    /**
     * @param Match[] $awayMatches
     * @return $this
     */
    public function setAwayMatches(array $awayMatches)
    {
        $this->awayMatches = new ArrayCollection($awayMatches);
        return $this;
    }

    /**
     * @return Match[]
     */
    public function getAllMatches()
    {
        return array_merge($this->homeMatches->toArray(), $this->awayMatches->toArray());
    }

    /**
     * @return Match[]
     */
    public function getAllGroupMatches()
    {
        $matches = $this->getAllMatches();
        return array_filter($matches, function (Match $match) {
            return $match->isGroup();
        });
    }

}
