<?php
namespace SoccerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use TeamBundle\Entity\Match as BaseMatch;

/**
 * @ORM\Entity(repositoryClass="SoccerBundle\Repository\MatchRepository", )
 * @ORM\Table(name="matches")
 */
class Match extends BaseMatch
{
    /**
     * @var int
     * @ORM\Column(type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var SoccerTeam
     * @ORM\ManyToOne(targetEntity="SoccerTeam", inversedBy="homeMatches")
     * @ORM\JoinColumn(name="home_team_id", nullable=false)
     */
    protected $homeTeam;

    /**
     * @var SoccerTeam
     * @ORM\ManyToOne(targetEntity="SoccerTeam", inversedBy="awayMatches")
     * @ORM\JoinColumn(name="away_team_id", nullable=false)
     */
    protected $awayTeam;

    /**
     * @var int
     * @ORM\Column(type="integer", name="score_home")
     */
    protected $scoreHome = 0;

    /**
     * @var int
     * @ORM\Column(type="integer", name="score_away")
     */
    protected $scoreAway = 0;

    /**
     * @var \DateTimeImmutable
     * @ORM\Column(type="date", name="date")
     */
    protected $date;

    /**
     * @var \DateTimeImmutable
     * @ORM\Column(type="time", name="time")
     */
    protected $time;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $arena;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $city;

    /**
     * @var Group
     * @ORM\ManyToOne(targetEntity="SoccerBundle\Entity\Group", inversedBy="matches")
     * @ORM\JoinColumn(name="group_id")
     */
    protected $group;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    protected $ended;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return SoccerTeam
     */
    public function getHomeTeam()
    {
        return $this->homeTeam;
    }

    /**
     * @param SoccerTeam $homeTeam
     * @return $this
     */
    public function setHomeTeam(SoccerTeam $homeTeam)
    {
        $this->homeTeam = $homeTeam;
        return $this;
    }

    /**
     * @return SoccerTeam
     */
    public function getAwayTeam()
    {
        return $this->awayTeam;
    }

    /**
     * @param SoccerTeam $awayTeam
     * @return $this
     */
    public function setAwayTeam(SoccerTeam $awayTeam)
    {
        $this->awayTeam = $awayTeam;
        return $this;
    }

    /**
     * @return int
     */
    public function getScoreHome()
    {
        return $this->scoreHome;
    }

    /**
     * @param int $scoreHome
     * @return $this
     */
    public function setScoreHome($scoreHome)
    {
        $this->scoreHome = $scoreHome;
        return $this;
    }

    /**
     * @return int
     */
    public function getScoreAway()
    {
        return $this->scoreAway;
    }

    /**
     * @param int $scoreAway
     * @return $this
     */
    public function setScoreAway($scoreAway)
    {
        $this->scoreAway = $scoreAway;
        return $this;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getDateTime()
    {
        if ($this->date === null || $this->time === null) {
            return null;
        }

        return \DateTimeImmutable::createFromFormat(
            'Y-m-d H:i:s',
            $this->date->format('Y-m-d') . ' ' . $this->time->format('H:i:s')
        );
    }

    /**
     * @return string
     */
    public function getArena()
    {
        return $this->arena;
    }

    /**
     * @param string $arena
     * @return $this
     */
    public function setArena($arena)
    {
        $this->arena = $arena;
        return $this;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     * @return $this
     */
    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isGroup()
    {
        return !!$this->getGroup();
    }

    /**
     * @return Group
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * @param Group $group
     * @return $this
     */
    public function setGroup(Group $group)
    {
        $this->group = $group;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isEnded()
    {
        return $this->ended;
    }

    /**
     * @param boolean $ended
     * @return $this
     */
    public function setEnded($ended)
    {
        $this->ended = $ended;
        return $this;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param \DateTimeImmutable $date
     * @return $this
     */
    public function setDate($date)
    {
        $this->date = $date;
        return $this;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * @param \DateTimeImmutable $time
     * @return $this
     */
    public function setTime($time)
    {
        $this->time = $time;
        return $this;
    }
}
