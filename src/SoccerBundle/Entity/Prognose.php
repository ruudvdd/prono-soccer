<?php
namespace SoccerBundle\Entity;

use TeamBundle\Entity\Prognose as BasePrognose;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="prognoses")
 */
class Prognose extends BasePrognose
{
    /**
     * @var Match
     * @ORM\Id()
     * @ORM\ManyToOne(targetEntity="SoccerBundle\Entity\Match")
     * @ORM\JoinColumn(name="match_id")
     */
    protected $match;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    protected $scoreHome;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    protected $scoreAway;

    /**
     * @return Match
     */
    public function getMatch()
    {
        return $this->match;
    }

    /**
     * @param Match $match
     * @return $this
     */
    public function setMatch($match)
    {
        $this->match = $match;
        return $this;
    }

    /**
     * @return int
     */
    public function getScoreHome()
    {
        return $this->scoreHome;
    }

    /**
     * @param int $scoreHome
     * @return $this
     */
    public function setScoreHome($scoreHome)
    {
        $this->scoreHome = $scoreHome;
        return $this;
    }

    /**
     * @return int
     */
    public function getScoreAway()
    {
        return $this->scoreAway;
    }

    /**
     * @param int $scoreAway
     * @return $this
     */
    public function setScoreAway($scoreAway)
    {
        $this->scoreAway = $scoreAway;
        return $this;
    }
}
