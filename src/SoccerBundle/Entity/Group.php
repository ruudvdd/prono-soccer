<?php
namespace SoccerBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="GroupRepository")
 * @ORM\Table(name="groups")
 */
class Group
{
    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=100)
     */
    protected $name;

    /**
     * @ORM\ManyToMany(targetEntity="SoccerBundle\Entity\SoccerTeam")
     * @ORM\JoinTable(name="groups_teams_linked",
     *      joinColumns={@ORM\JoinColumn(name="group_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="team_id", referencedColumnName="id", unique=true)}
     * )
     * @var ArrayCollection
     */
    protected $teams;

    /**
     * @ORM\OneToMany(targetEntity="SoccerBundle\Entity\Match", mappedBy="group")
     * @var ArrayCollection
     */
    protected $matches;

    public function __construct()
    {
        $this->teams = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return SoccerTeam[]
     */
    public function getTeams()
    {
        return $this->teams->toArray();
    }

    /**
     * @param array|ArrayCollection $teams
     * @return $this
     */
    public function setTeams($teams)
    {
        if (is_array($teams)) {
            $teams = new ArrayCollection($teams);
        }

        $this->teams = $teams;
        return $this;
    }

    /**
     * @param SoccerTeam $team
     * @return $this
     */
    public function addTeam(SoccerTeam $team)
    {
        $this->teams->add($team);
        return $this;
    }

    /**
     * @param SoccerTeam $team
     * @return $this
     */
    public function removeTeam(SoccerTeam $team)
    {
        $this->teams->removeElement($team);
        return $this;
    }

    /**
     * @return Match[]
     */
    public function getAllMatches()
    {
        $matches = [];
        foreach ($this->getTeams() as $team) {
            $homeMatches = $team->getHomeMatches();
            $matches = array_merge($matches, $homeMatches);
        }
        return $matches;
    }

    public function getAllMatchesOrderedByDateTime()
    {
        $matches = $this->getAllMatches();
        usort($matches, function (Match $match1, Match $match2) {
            return $match1->getDateTime() < $match2->getDateTime() ? -1 : 1;
        });
        return $matches;
    }
}
