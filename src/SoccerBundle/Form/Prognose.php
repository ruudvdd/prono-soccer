<?php
namespace SoccerBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints;

class Prognose extends AbstractType
{
    /**
     * @inheritdoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('scoreHome', IntegerType::class, [
                'attr' => [
                    'min' => 0
                ],
                'constraints' => [
                    new Constraints\NotBlank(),
                    new Constraints\Range(['min' => 0])
                ]
            ])
            ->add('scoreAway', IntegerType::class, [
                'attr' => [
                    'min' => 0
                ],
                'constraints' => [
                    new Constraints\NotBlank(),
                    new Constraints\Range(['min' => 0])
                ]
            ])
            ->add('submit', SubmitType::class, ['label' => 'Opslaan']);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'SoccerBundle\Entity\Prognose',
            'attr' => ['novalidate' => 'novalidate']
        ]);
    }
}
