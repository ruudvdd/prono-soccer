<?php
namespace SoccerBundle\Service;

use Doctrine\Common\Persistence\ObjectManager;
use SoccerBundle\Entity\Group;
use SoccerBundle\Entity\Match;

class MatchPersistTool
{
    /**
     * @var ObjectManager
     */
    protected $em;

    /**
     * @param ObjectManager $manager
     */
    public function __construct(ObjectManager $manager)
    {
        $this->em = $manager;
    }

    /**
     * @param Match[] $matches
     * @param array $formData
     * @param bool $away
     * @return array
     */
    public function fillInAndPersistGroupMatches(array $matches, array $formData, $away = true)
    {
        foreach ($matches as $keySuffix => $match) {
            $filledIn = true;

            if (empty($formData['dateTime_' . $keySuffix])) {
                $filledIn = false;
            }

            if (empty($formData['arena_' . $keySuffix])) {
                $filledIn = false;
            }

            if (empty($formData['city_' . $keySuffix])) {
                $filledIn = false;
            }

            if (!$filledIn) {
                continue;
            }

            $dateTimeInput = $formData['dateTime_' . $keySuffix];
            $date = $time = \DateTimeImmutable::createFromFormat('Y-m-d\TH:i', $dateTimeInput);
            $match->setDate($date);
            $match->setTime($time);

            $arena = $formData['arena_' . $keySuffix];
            $match->setArena($arena);

            $city = $formData['city_' . $keySuffix];
            $match->setCity($city);

            $this->em->persist($match);
        }
        $this->em->flush();
    }

    /**
     * @param Group[] $groups
     * @param bool $away
     * @return Match[]
     */
    public function getAllMatchesForGroups(array $groups, $away)
    {
        $matches = [];
        foreach ($groups as $group) {
            $matches = array_merge(
                $matches,
                $this->getMatchesForGroup($group, $away)
            );
        }
        return $matches;
    }

    /**
     * @param Group $group
     * @param bool $away
     * @return Match[]
     */
    private function getMatchesForGroup(Group $group, $away)
    {
        $groupMatches = [];
        $teams = $group->getTeams();
        $matchRepository = $this->em->getRepository('SoccerBundle:Match');

        foreach ($teams as $homeTeam) {
            foreach ($teams as $awayTeam) {
                $matchInRepository = true;
                if ($homeTeam === $awayTeam) {
                    continue;
                }

                $match = $matchRepository->findBetween($homeTeam, $awayTeam, true);
                if ($match === null) {
                    $matchInRepository = false;
                    $match = new Match();
                    $match->setGroup($group)
                        ->setHomeTeam($homeTeam)
                        ->setAwayTeam($awayTeam)
                        ->setEnded(0);
                }

                if (!$away) {
                    $oppositeMatchKey = $awayTeam->getId() . '_' . $homeTeam->getId();
                    if (isset($groupMatches[$oppositeMatchKey])) {
                        if ($matchInRepository) {
                            unset($groupMatches[$oppositeMatchKey]);
                        } else {
                            continue;
                        }
                    }
                }

                $groupMatches[$homeTeam->getId() . '_' . $awayTeam->getId()] = $match;
            }
        }

        return $groupMatches;
    }
}
