<?php
namespace SoccerBundle\Prognose;

use SoccerBundle\Entity\Prognose;

class PrognoseToPoints
{
    /**
     * @var PointsCalculator
     */
    protected $pointsCalculator;

    /**
     * @param PointsCalculator $pointsCalculator
     */
    public function __construct(PointsCalculator $pointsCalculator)
    {
        $this->pointsCalculator = $pointsCalculator;
    }

    /**
     * @param Prognose $prognose
     */
    public function getPoints(Prognose $prognose)
    {
        $match = $prognose->getMatch();
        $this->pointsCalculator->calculate(
            $prognose->getScoreHome(),
            $prognose->getScoreAway(),
            $match->getScoreHome(),
            $match->getScoreHome()
        );
    }
}
