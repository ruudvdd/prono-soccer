<?php
namespace SoccerBundle\Prognose;

class PointsCalculator
{
    /**
     * @param int $matchScoreHome
     * @param int $matchScoreAway
     * @param int $prognoseScoreHome
     * @param int $prognoseScoreAway
     * @return int
     */
    public function calculate($matchScoreHome, $matchScoreAway, $prognoseScoreHome, $prognoseScoreAway)
    {

        $homeScoreDifference = $prognoseScoreHome - $matchScoreHome;
        $awayScoreDifference = $prognoseScoreAway - $matchScoreAway;

        if ($homeScoreDifference === 0 && $awayScoreDifference === 0) {
            return 3;
        }

        $matchScoreDifference = $prognoseScoreHome - $prognoseScoreAway;
        $prognoseScoreDifference = $matchScoreHome - $matchScoreAway;

        if ($matchScoreDifference > 0 && $prognoseScoreDifference > 0) {
            return 1;
        }

        if ($matchScoreDifference < 0 && $prognoseScoreDifference < 0) {
            return 1;
        }

        if ($matchScoreDifference === 0 && $prognoseScoreDifference === 0) {
            return 1;
        }

        return 0;
    }
}
