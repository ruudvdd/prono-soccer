<?php
namespace SoccerBundle\Value;

use SoccerBundle\Entity\SoccerTeam;

class TeamGroupScore
{
    /**
     * @var SoccerTeam
     */
    protected $team;

    /**
     * @var int
     */
    protected $played;

    /**
     * @var int
     */
    protected $wins;

    /**
     * @var int
     */
    protected $loses;

    /**
     * @var int
     */
    protected $goalsFor;

    /**
     * @var int
     */
    protected $goalsAgainst;

    /**
     * @var int
     */
    protected $goalDifference;

    /**
     * @var int
     */
    protected $points;

    /**
     * @var int
     */
    protected $draws;

    /**
     * @return SoccerTeam
     */
    public function getTeam()
    {
        return $this->team;
    }

    /**
     * @param SoccerTeam $team
     * @return $this
     */
    public function setTeam(SoccerTeam $team)
    {
        $this->team = $team;
        return $this;
    }

    /**
     * @return int
     */
    public function getPlayed()
    {
        return $this->played;
    }

    /**
     * @param int $played
     * @return $this
     */
    public function setPlayed($played)
    {
        $this->played = $played;
        return $this;
    }

    /**
     * @param int $played
     * @return $this
     */
    public function addPlayed($played)
    {
        $this->played += $played;
        return $this;
    }

    /**
     * @return int
     */
    public function getWins()
    {
        return $this->wins;
    }

    /**
     * @param int $wins
     * @return $this
     */
    public function setWins($wins)
    {
        $this->wins = $wins;
        return $this;
    }

    /**
     * @param int $wins
     * @return $this
     */
    public function addWins($wins)
    {
        $this->wins += $wins;
        return $this;
    }

    /**
     * @return int
     */
    public function getDraws()
    {
        return $this->draws;
    }

    /**
     * @param int $draws
     * @return $this
     */
    public function setDraws($draws)
    {
        $this->draws = $draws;
        return $this;
    }

    /**
     * @param int $draws
     * @return $this
     */
    public function addDraws($draws)
    {
        $this->draws += $draws;
        return $this;
    }

    /**
     * @return int
     */
    public function getLoses()
    {
        return $this->loses;
    }

    /**
     * @param int $loses
     * @return $this
     */
    public function setLoses($loses)
    {
        $this->loses = $loses;
        return $this;
    }

    /**
     * @param int $loses
     * @return $this
     */
    public function addLoses($loses)
    {
        $this->loses += $loses;
        return $this;
    }

    /**
     * @return int
     */
    public function getGoalsFor()
    {
        return $this->goalsFor;
    }

    /**
     * @param int $goalsFor
     * @return $this
     */
    public function setGoalsFor($goalsFor)
    {
        $this->goalsFor = $goalsFor;
        return $this;
    }

    /**
     * @param int $goalsFor
     * @return $this
     */
    public function addGoalsFor($goalsFor)
    {
        $this->goalsFor += $goalsFor;
        return $this;
    }

    /**
     * @return int
     */
    public function getGoalsAgainst()
    {
        return $this->goalsAgainst;
    }

    /**
     * @param int $goalsAgainst
     * @return $this
     */
    public function setGoalsAgainst($goalsAgainst)
    {
        $this->goalsAgainst = $goalsAgainst;
        return $this;
    }

    /**
     * @param int $goalsAgainst
     * @return $this
     */
    public function addGoalsAgainst($goalsAgainst)
    {
        $this->goalsAgainst += $goalsAgainst;
        return $this;
    }

    /**
     * @return int
     */
    public function getGoalDifference()
    {
        return $this->goalDifference;
    }

    /**
     * @param int $goalDifference
     * @return $this
     */
    public function setGoalDifference($goalDifference)
    {
        $this->goalDifference = $goalDifference;
        return $this;
    }

    /**
     * @param int $goalDifference
     * @return $this
     */
    public function addGoalDifference($goalDifference)
    {
        $this->goalDifference += $goalDifference;
        return $this;
    }

    /**
     * @return int
     */
    public function getPoints()
    {
        return $this->points;
    }

    /**
     * @param int $points
     * @return $this
     */
    public function setPoints($points)
    {
        $this->points = $points;
        return $this;
    }

    /**
     * @param int $points
     * @return $this
     */
    public function addPoints($points)
    {
        $this->points += $points;
        return $this;
    }
}
