<?php
namespace SoccerBundle\Value;

use SoccerBundle\Entity\Group;

class GroupScoreBoard
{
    const ORDER_POINTS = 'points';

    const ORDER_DIRECTION_ASC = 'asc';
    const ORDER_DIRECTION_DESC = 'desc';

    /**
     * @var TeamGroupScore[]
     */
    protected $groupScores = [];

    /**
     * @var Group
     */
    protected $group;

    /**
     * @return TeamGroupScore[]
     */
    public function getGroupScores()
    {
        return $this->groupScores;
    }

    /**
     * @param TeamGroupScore[] $groupScores
     * @return $this
     */
    public function setGroupScores($groupScores)
    {
        $this->groupScores = $groupScores;
        return $this;
    }

    /**
     * @param string $order
     * @param string $orderDirection
     * @return TeamGroupScore[]
     */
    public function getOrderedScores($order = self::ORDER_POINTS, $orderDirection = self::ORDER_DIRECTION_ASC)
    {
        $scores = $this->getGroupScores();

        if (!$this->orderIsAllowed($order)) {
            throw new \LogicException('Order by \'' . $order . '\' is not allowed');
        }

        if (!$this->orderDirectionIsAllowed($orderDirection)) {
            throw new \LogicException('Order direction \'' . $orderDirection . '\' is not allowed');
        }

        $getter = $this->allowedOrders()[$order];
        usort($scores, function (TeamGroupScore $score1, TeamGroupScore $score2) use ($getter, $orderDirection) {
            $value1 = $score1->{$getter}();
            $value2 = $score2->{$getter}();

            $compare = $value1 < $value2;
            if ($orderDirection === self::ORDER_DIRECTION_DESC) {
                $compare = !$compare;
            }

            return $compare ? 1 : -1;
        });

        return $scores;
    }

    /**
     * @param string $order
     * @return bool
     */
    protected function orderIsAllowed($order)
    {
        return isset($this->allowedOrders()[$order]);
    }

    /**
     * @return array
     */
    protected function allowedOrders()
    {
        return [
            self::ORDER_POINTS => 'getPoints'
        ];
    }

    private function orderDirectionIsAllowed($orderDirection)
    {
        return in_array($orderDirection, [
            self::ORDER_DIRECTION_ASC,
            self::ORDER_DIRECTION_DESC
        ]);
    }

    /**
     * @return Group
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * @param Group $group
     * @return $this
     */
    public function setGroup($group)
    {
        $this->group = $group;
        return $this;
    }
}
