<?php
namespace SoccerBundle\Command;

use SoccerBundle\Entity\Match;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SoccerMatchResultCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('soccer:match:result')
            ->addArgument('match', InputArgument::REQUIRED)
            ->addArgument('home-score', InputArgument::REQUIRED)
            ->addArgument('away-score', InputArgument::REQUIRED);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $matchId = $input->getArgument('match');
        $em = $this->getContainer()->get('doctrine')->getManager();

        $matchRepository = $em->getRepository('SoccerBundle:Match');

        /** @var Match $match */
        $match = $matchRepository->find($matchId);

        $homeScore = $input->getArgument('home-score');
        $awayScore = $input->getArgument('away-score');

        $match->setEnded(1);
        $match->setScoreHome($homeScore);
        $match->setScoreAway($awayScore);

        $em->flush();

        $command = $this->getApplication()->find('soccer:user:calculate-scores');
        $command->run(new ArrayInput([]), $output);
    }


}
