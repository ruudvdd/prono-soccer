<?php

namespace SoccerBundle\Command;

use SoccerBundle\Entity\Match;
use SoccerBundle\Entity\Prognose;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SoccerUserCalculateScoresCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('soccer:user:calculate-scores')
            ->setDescription('Re-calculate the scores of all users')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $doctrine = $this->getContainer()->get('doctrine');
        $manager = $doctrine->getManager();
        $userRepository = $doctrine->getRepository('AppBundle:User');

        $users = $userRepository->findAll();

        $matchRepository = $doctrine->getRepository('SoccerBundle:Match');
        $endedMatches = $matchRepository->findAllEnded();

        foreach ($users as $user) {
            $score = 0;
            foreach ($endedMatches as $endedMatch) {
                $prognose = $user->getPrognose($endedMatch);
                if ($prognose === null) {
                    continue;
                }

                $score += $this->calculate($endedMatch, $prognose);
            }

            $user->setScore($score);
            $manager->persist($user);
        }

        $manager->flush();
        $output->writeln($this->getHelper('formatter')->formatBlock('Done', 'info', true));
    }

    /**
     * @param Match $endedMatch
     * @param Prognose $prognose
     * @return int
     */
    private function calculate(Match $endedMatch, Prognose $prognose)
    {
        $pointsCalculator = $this->getContainer()->get('soccer.prognose.points_calculator');
        return $pointsCalculator->calculate(
            $endedMatch->getScoreHome(),
            $endedMatch->getScoreAway(),
            $prognose->getScoreHome(),
            $prognose->getScoreAway()
        );
    }

}
