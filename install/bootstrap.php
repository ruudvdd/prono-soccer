<?php

$root = '../../';

$bootstrapTarget = 'web/bundles/bootstrap';
if (!file_exists($bootstrapTarget)) {
    symlink($root . 'vendor/twbs/bootstrap/dist', $bootstrapTarget);
}