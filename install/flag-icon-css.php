<?php

$root = '../../';

$bootstrapTarget = 'web/bundles/flag-icon-css';
if (!file_exists($bootstrapTarget)) {
    symlink($root . 'app/Resources/flag-icon-css', $bootstrapTarget);
}